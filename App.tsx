import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import HomeScreen from "./screens/HomeScreen";
import BookScreen from "./screens/BookScreen";
import CoverScreen from "./screens/CoverScreen";

export default function App() {

    const Stack = createNativeStackNavigator();

    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="Book" component={BookScreen} />
                <Stack.Screen name="Cover" component={CoverScreen} />

            </Stack.Navigator>

        </NavigationContainer>
    )
}


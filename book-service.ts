import axios from "axios";
import { Book } from "./entitites";

export async function fetchBook(data : string) {
    const response = await axios.get<Book>('https://openlibrary.org/works/'+data+'.json');
    return response.data;
}

/* export async function fetchCover(data : string) {
    const response = await axios.get<string>('https://covers.openlibrary.org/b/olid/'+data+'-L.jpg');
    console.log('url : '+'https://covers.openlibrary.org/b/olid/'+data+'-L.jpg')
    return response.data;
} */
export interface Book {
    key?:number;
    title?:string;
    author?:string;
}

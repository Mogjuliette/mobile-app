import { StatusBar } from 'expo-status-bar';
import { Button, Image, StyleSheet, Text, View } from 'react-native';
import { Book } from '../entitites';
import { useEffect, useState } from 'react';
import { fetchBook } from '../book-service';

export default function BookScreen() {

  const [olid, setOlid] = useState<string>('OL25624771M');
  const [book, setBook] = useState<Book>();

  useEffect(() => {
    fetchBook(olid)
        .then(data => {
            setBook(data);
            console.log('book :'+book)
        }).catch(err => console.log(err));
}, [olid]);

//Ceci est un commentaire mais j'ai pas d'idées
//Ceci est un autre commentaire, mais j'ai toujours pas d'idées

  return (
    <View style={styles.container}>
      <Image source={{uri : 'https://covers.openlibrary.org/b/olid/'+olid+'.jpg'}} style={styles.img}></Image>
      <Text >Mon livre {book?.title} </Text>
      <Button title="Changer de livre" onPress={() => setOlid('OL34491166M')} />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  img: {
    width: '100%',
    height: 400
}
});
import { StatusBar } from 'expo-status-bar';
import { Image, StyleSheet, Text, View } from 'react-native';
import {  useState } from 'react';

export default function BookScreen() {

  const [olid, setOlid]  = useState<string>('OL38237074M-L');


  return (
    <View style={styles.container}>
      <Text >Couverture du  livre :</Text>
      <Image source={{uri : 'https://covers.openlibrary.org/b/olid/'+olid+'.jpg'}} style={styles.img}></Image>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  img: {
    width: '100%',
    height: 400
}
});
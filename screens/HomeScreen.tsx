import { useNavigation } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, Text, View } from 'react-native';

export default function HomeScreen() {

const navigation = useNavigation<any>();
  return (
    <View style={styles.container}>
      <Text>Une appli de livres</Text>
      <Button title="Book" onPress={() => navigation.navigate("Book")} />
      <Button title="Cover" onPress={() => navigation.navigate("Cover")} />

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  img: {
        width: '100%',
        height: 400
    }

});